package pl.imiajd.sulikowski;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Komputer implements Cloneable, Comparable<Komputer> {
    private String nazwa;
    private LocalDate dataProdukcji;

    public Komputer(String _nazwa, LocalDate data_prod) {
        nazwa = _nazwa;
        dataProdukcji = data_prod;
    }

    public boolean equals(Komputer other) {
        return nazwa.equals(other.nazwa) && dataProdukcji.equals(other.dataProdukcji);
    }

    public String toString() {
        String result = new String();
        result += this.getClass().getSimpleName();
        result += " [";
        result += nazwa;
        result += " ";
        result += dataProdukcji.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        result += "]";
        return result;
    }

    public int compareTo(Komputer other) {
        int order = nazwa.compareTo(other.nazwa);
        if(order == 0) {
            order = dataProdukcji.compareTo(other.dataProdukcji);
        }
        return order;
    }

    @Override
    public Komputer clone()
    {
        return new Komputer(nazwa + " (kopia)", dataProdukcji);
    }
}
