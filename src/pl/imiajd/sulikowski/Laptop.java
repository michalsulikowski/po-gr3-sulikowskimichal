package pl.imiajd.sulikowski;


import java.time.LocalDate;

public class Laptop extends Komputer {
    boolean czyApple;

    public Laptop(String _nazwa, LocalDate data_prod, boolean czy_apple) {
        super(_nazwa, data_prod);
        czyApple = czy_apple;
    }

    public int compareTo(Laptop other) {
        int order = super.compareTo(other);
        if(order != 0) {
            return order;
        }
        if(czyApple && !other.czyApple) {
            return -1;
        }
        else if(!czyApple && other.czyApple) {
            return 1;
        }
        return 0;
    }
}