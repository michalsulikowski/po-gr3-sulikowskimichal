package pl.imiajd.sulikowski;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Osoba implements Cloneable, Comparable<Osoba> {
    protected String nazwisko;
    protected LocalDate dataUrodzenia;

    public Osoba(String nazw, LocalDate data_ur) {
        nazwisko = nazw;
        dataUrodzenia = data_ur;
    }

    public String toString() {
        String result = new String();
        result += this.getClass().getSimpleName();
        result += " [";
        result += nazwisko;
        result += " ";
        result += dataUrodzenia.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        result += "]";
        return result;
    }

    public boolean equals(Osoba other) {
        return nazwisko.equals(other.nazwisko) && dataUrodzenia.equals(other.dataUrodzenia);
    }

    public int compareTo(Osoba other) {
        int order = nazwisko.compareTo(other.nazwisko);
        if(order == 0) {
            order = dataUrodzenia.compareTo(other.dataUrodzenia);
        }
        return order;
    }
}