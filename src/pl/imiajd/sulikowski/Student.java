package pl.imiajd.sulikowski;

import java.time.LocalDate;
import pl.imiajd.sulikowski.Osoba;

public class Student extends Osoba implements Cloneable, Comparable<Osoba> {
    private double sredniaOcen;

    public Student(String nazw, LocalDate data_ur, double srednia) {
        super(nazw, data_ur);
        sredniaOcen = srednia;
    }

    public int compareTo(Osoba other_o) {
        Student other = (Student)other_o;
        int order = super.compareTo(other);
        if(order != 0) {
            return order;
        }
        if(sredniaOcen < other.sredniaOcen) {
            return -1;
        }
        else if(sredniaOcen > other.sredniaOcen) {
            return 1;
        }
        return 0;
    }
}