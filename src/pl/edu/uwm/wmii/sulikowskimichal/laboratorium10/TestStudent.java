package pl.edu.uwm.wmii.sulikowskimichal.laboratorium10;


import pl.imiajd.sulikowski.Student;

import java.util.ArrayList;
import java.time.LocalDate;
import java.util.Comparator;

public class TestStudent {
    public static void main(String[] args) {
        ArrayList<Student> grupa = new ArrayList<>();
        grupa.add(new Student("Sulikowski", LocalDate.of(1999, 7, 2), 3.0));
        grupa.add(new Student("Sulikowski", LocalDate.of(1999, 5, 12), 4.42));
        grupa.add(new Student("Kowalski", LocalDate.of(1999, 10, 2), 2.71));
        grupa.add(new Student("Nowak", LocalDate.of(1999, 10, 2), 3.99));
        grupa.add(new Student("Konstantynopolitanczykowianeczkowaty", LocalDate.of(1971, 12, 18), 5.0));

        System.out.println(grupa);
        grupa.sort(Comparator.naturalOrder());
        System.out.println(grupa);
    }
}
