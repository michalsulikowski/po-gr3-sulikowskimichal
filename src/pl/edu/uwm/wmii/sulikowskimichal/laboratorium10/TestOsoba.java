package pl.edu.uwm.wmii.sulikowskimichal.laboratorium10;

import pl.imiajd.sulikowski.Osoba;

import java.util.ArrayList;
import java.time.LocalDate;
import java.util.Comparator;

public class TestOsoba {
    public static void main(String[] args) {
        ArrayList<Osoba> grupa = new ArrayList<>();
        grupa.add(new Osoba("Sulikowski", LocalDate.of(1999, 7, 2)));
        grupa.add(new Osoba("Sulikowski", LocalDate.of(1999, 5, 12)));
        grupa.add(new Osoba("Kowalski", LocalDate.of(1999, 10, 2)));
        grupa.add(new Osoba("Nowak", LocalDate.of(1999, 10, 2)));
        grupa.add(new Osoba("Konstantynopolitanczykowianeczkowaty", LocalDate.of(1971, 12, 18)));

        System.out.println(grupa);
        grupa.sort(Comparator.naturalOrder());
        System.out.println(grupa);
    }
}
