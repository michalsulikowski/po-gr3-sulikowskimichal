package pl.edu.uwm.wmii.sulikowskimichal.laboratorium06;

import java.util.ArrayList;
import java.util.Arrays;

class IntegerSet {
    private boolean[] numbers = new boolean[100];

    public void insertElement(int num) {
        numbers[num-1] = true;
    }

    public void removeElement(int num) {
        numbers[num-1] = false;
    }

    public boolean contains(int num) {
        return numbers[num-1];
    }

    public static IntegerSet union(IntegerSet first, IntegerSet second) {
        IntegerSet result = new IntegerSet();
        for(int i=1; i<=100; i++) {
            if(first.contains(i) || second.contains(i)) {
                result.insertElement(i);
            }
        }
        return result;
    }

    public static IntegerSet intersection(IntegerSet first, IntegerSet second) {
        IntegerSet result = new IntegerSet();
        for(int i=1; i<=100; i++) {
            if(first.contains(i) && second.contains(i)) {
                result.insertElement(i);
            }
        }
        return result;
    }

    public boolean equals(IntegerSet other) {
        for(int i=1; i<=100; i++) {
            if(contains(i) != other.contains(i)) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        String result = new String("[");
        for(int i=1; i<=100; i++) {
            if(contains(i)) {
                result += i + ", ";
            }
        }
        if(!result.isEmpty()) {
            result = result.substring(0, result.length() - 2);
        }
        result += "]";
        return result;
    }
}

public class Zadanie2 {
    public static void main(String[] args) {
        IntegerSet set = new IntegerSet();
        set.insertElement(5);
        set.insertElement(100);
        set.insertElement(1);
        System.out.println(set);

        IntegerSet other = new IntegerSet();
        other.insertElement(7);
        other.insertElement(42);
        other.insertElement(100);
        System.out.println(IntegerSet.union(set, other));
        System.out.println(IntegerSet.intersection(set, other));
        System.out.println(set.equals(other));

        IntegerSet left_eq = new IntegerSet();
        left_eq.insertElement(50);
        IntegerSet right_eq = new IntegerSet();
        right_eq.insertElement(50);

        System.out.println(left_eq.equals(right_eq));
    }
}
