package pl.edu.uwm.wmii.sulikowskimichal.laboratorium06;

import java.util.ArrayList;
import java.util.Arrays;

class RachunekBankowy {
    private static double rocznaStopaProcentowa = 0.04;
    private double saldo = 0.0;

    public RachunekBankowy(double poczatkowe_saldo) {
        saldo = poczatkowe_saldo;
    }

    public void wypiszSaldo() {
        System.out.println(saldo);
    }

    public void obliczMiesieczneOdsetki() {
        saldo += (saldo * rocznaStopaProcentowa) / 12;
    }

    public static void setRocznaStopaProcentowa(double new_perc) {
        rocznaStopaProcentowa = new_perc;
    }
}

public class Zadanie1 {
    public static void main(String[] args) {
        RachunekBankowy saver1 = new RachunekBankowy(2000.0);
        RachunekBankowy saver2 = new RachunekBankowy(3000.0);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        saver1.wypiszSaldo();
        saver2.wypiszSaldo();
        System.out.println("");
        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        saver1.wypiszSaldo();
        saver2.wypiszSaldo();
    }
}
