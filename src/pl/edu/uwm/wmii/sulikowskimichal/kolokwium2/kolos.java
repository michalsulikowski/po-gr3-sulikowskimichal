package pl.edu.uwm.wmii.sulikowskimichal.kolokwium2;

import pl.imiajd.sulikowski.Komputer;
import pl.imiajd.sulikowski.Laptop;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;


public class kolos {
    // Funkcja usuwa co n-ty komputer liczac od 1.
    public static void redukuj(LinkedList<Komputer> komputery, int n) {
        for (int i = n - 1; i < komputery.size(); i += n - 1) {
            komputery.remove(i);

        }
    }

    public static void main(String[] args) {
        ArrayList<Komputer> grupa = new ArrayList<>();
        Komputer komp = new Komputer("Komputer Michala", LocalDate.of(2017, 7, 1));
        grupa.add(komp);
        grupa.add(komp.clone());
        grupa.add(new Laptop("Mac", LocalDate.of(1999, 10, 2), true));
        grupa.add(new Laptop("Dell", LocalDate.of(1999, 10, 2), false));
        grupa.add(new Laptop("serwer teams", LocalDate.of(1971, 12, 18), false));

        System.out.println(grupa);
        grupa.sort(Comparator.naturalOrder());
        System.out.println(grupa);

        LinkedList<Komputer> lista_komputerow = new LinkedList<>();
        lista_komputerow.addAll(grupa);
        redukuj(lista_komputerow, 2);
        System.out.println(lista_komputerow);
    }
}
