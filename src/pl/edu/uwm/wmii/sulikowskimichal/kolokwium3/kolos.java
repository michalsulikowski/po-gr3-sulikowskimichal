package pl.edu.uwm.wmii.sulikowskimichal.kolokwium3;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;

public class kolos {

    public static void main(String[] args) {
        ArrayList<Student> studenci = new ArrayList<>();
        studenci.add(new Student("Nowak", LocalDate.of(2007, 9, 1), 2.0));
        studenci.add(new Student("Kowalski", LocalDate.of(1999, 2, 17), 4.5));
        studenci.add(new Student("Kozlowski", LocalDate.of(1999, 2, 17), 3.0));
        studenci.add(new Student("Nowak", LocalDate.of(2006, 5, 22), 5.0));
        Stypendium stypendium = new Stypendium(studenci);

        ArrayList<Student> studenci_2 = stypendium.getStudenci();
        System.out.println(studenci_2);
        studenci_2.sort(Comparator.naturalOrder());
        System.out.println(studenci_2);

        ArrayList<Student> stypendialni = new ArrayList<>();
        System.out.println("Kwoty stypendiow:");
        for(int i=0; i<studenci_2.size(); i++) {
            Student s = studenci_2.get(i);
            double kwota = Stypendium.KwotaStypendium(s);
            System.out.println(s.getNazwa() + ": " + kwota);
            if(kwota > 0.0) {
                stypendialni.add(s);
            }
        }
        System.out.println("Studenci ktorzy dostana stypendium:");
        System.out.println(stypendialni);
    }
}
