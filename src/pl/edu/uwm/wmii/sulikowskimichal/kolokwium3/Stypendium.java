package pl.edu.uwm.wmii.sulikowskimichal.kolokwium3;

import java.time.LocalDate;
import java.util.ArrayList;

public class Stypendium {
    private static double kwotaStypendium = 500;
    private ArrayList<Student> studenci;

    public Stypendium(ArrayList<Student> nowi_studenci) {
        studenci = nowi_studenci;
    }

    public static void setKwotaStypendium(double nowa_kwota) {
        kwotaStypendium = nowa_kwota;
    }

    public ArrayList<Student> getStudenci() {
        return studenci;
    }

    public static double KwotaStypendium(Student s) {
        if(s.getOcena() == 5.0 && s.getDataUrodziny().isAfter(LocalDate.of(2005, 12, 31))) {
            return kwotaStypendium;
        }
        return 0;
    }
}
