package pl.edu.uwm.wmii.sulikowskimichal.kolokwium3;

import java.time.LocalDate;

public class Student implements Cloneable, Comparable<Student> {
    private String nazwa;
    private LocalDate dataUrodziny;
    private double ocena;

    public Student(String _nazwa, LocalDate _dataUrodzin, double _ocena) {
        nazwa = _nazwa;
        dataUrodziny = _dataUrodzin;
        ocena = _ocena;
    }

    public String getNazwa() {
        return nazwa;
    }

    public LocalDate getDataUrodziny() {
        return dataUrodziny;
    }

    public double getOcena() {
        return ocena;
    }

    public String toString() {
        return getNazwa() + ", data urodzenia: " + getDataUrodziny() + ", ocena: " + getOcena();
    }

    @Override public int compareTo(Student other) {
        int c = getDataUrodziny().compareTo(other.getDataUrodziny());
        if(c != 0) return c;

        c = getNazwa().compareTo(other.getNazwa());
        if(c != 0) return c;

        return Double.compare(getOcena(), other.getOcena());
    }
}