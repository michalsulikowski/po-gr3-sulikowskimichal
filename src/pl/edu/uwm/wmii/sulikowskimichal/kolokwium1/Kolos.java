package pl.edu.uwm.wmii.sulikowskimichal.kolokwium1;


import java.util.Scanner;
import java.lang.Character;

public class Kolos {
    public static void count_numbers() {
        Scanner reader = new Scanner(System.in);

        System.out.print("Podaj, ile liczb nalezy wczytac: ");
        int n = reader.nextInt();

        int smaller = 0;
        int bigger = 0;
        int equal = 0;

        for(int i=0; i<n; i++) {
            double num = reader.nextDouble();
            if (num < -5) {
                smaller++;
            }
            else if (num == -5) {
                equal++;
            }
            else {
                bigger++;
            }
        }

        System.out.print("Mniejsze od -5: ");
        System.out.println(smaller);

        System.out.print("Rowne -5: ");
        System.out.println(equal);

        System.out.print("Wieksze od -5: ");
        System.out.println(bigger);

    }

    public static String delete(String s, char c) {
        String res = new String();

        boolean has_appeared_once = false;
        for(int i=0; i<s.length(); i++) {
            char ch = s.charAt(i);
            if(Character.toLowerCase(ch) == c) {
                if(has_appeared_once) {
                    continue;
                }
                has_appeared_once = true;
            }
            res += ch;
        }

        return res;
    }

    public static void main(String[] args) {
        count_numbers();
        String s = delete("Intellij IDE", 'i');
        System.out.println(s);
        if(s.equals("Intellj DE")) {
            System.out.println("Test OK");
        }
    }
}
