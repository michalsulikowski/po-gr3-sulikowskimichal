package pl.edu.uwm.wmii.sulikowskimichal.laboratorium05;

import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie5 {
    public static void reverse(ArrayList<Integer> a) {
        for(int i = 0; i < a.size()/2; i++) {
            Integer temp = a.get(i);
            a.set(i, a.get(a.size() - i - 1));
            a.set(a.size() - i - 1, temp);
        }
    }

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList(Arrays.asList(1, 2, 3, 4, 5));
        ArrayList<Integer> a2 = new ArrayList(Arrays.asList(1, 2, 3, 4, 5, 6));
        reverse(a);
        reverse(a2);
        ArrayList<Integer> expected = new ArrayList<Integer>(Arrays.asList(5, 4, 3, 2, 1));
        ArrayList<Integer> expected2 = new ArrayList<Integer>(Arrays.asList(6, 5, 4, 3, 2, 1));
        System.out.println(a);
        System.out.println(expected);
        System.out.println(a.equals(expected));
        System.out.println(a2);
        System.out.println(expected2);
        System.out.println(a2.equals(expected2));
    }
}
