package pl.edu.uwm.wmii.sulikowskimichal.laboratorium05;

import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie2 {
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> res = new ArrayList<>();
        while(a.size() > 0 || b.size() > 0) {
            if(a.size() > 0) {
                res.add(a.remove(0));
            }
            if(b.size() > 0) {
                res.add(b.remove(0));
            }
        }
        return res;
    }

    public static void main(String[] args) {
        ArrayList<Integer> left = new ArrayList(Arrays.asList(1, 2, 3));
        ArrayList<Integer> right = new ArrayList(Arrays.asList(4, 5, 6, 7, 8));
        ArrayList<Integer> merged = merge(left, right);
        ArrayList<Integer> expected = new ArrayList<Integer>(Arrays.asList(1, 4, 2, 5, 3, 6, 7, 8));
        System.out.println(merged);
        System.out.println(expected);
        System.out.println(merged.equals(expected));
    }
}
