package pl.edu.uwm.wmii.sulikowskimichal.laboratorium05;

import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie1 {
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> res = new ArrayList<>();
        res.addAll(a);
        res.addAll(b);
        return res;
    }

    public static void main(String[] args) {
        ArrayList<Integer> left = new ArrayList(Arrays.asList(1, 2, 3));
        ArrayList<Integer> right = new ArrayList(Arrays.asList(4, 5, 6));
        ArrayList<Integer> joined = append(left, right);
        ArrayList<Integer> expected = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6));
        System.out.println(joined.equals(expected));
    }
}
