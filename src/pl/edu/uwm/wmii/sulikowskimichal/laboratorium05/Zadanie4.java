package pl.edu.uwm.wmii.sulikowskimichal.laboratorium05;

import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie4 {
    public static ArrayList<Integer> reversed(ArrayList<Integer> a) {
        ArrayList<Integer> res = new ArrayList<>();
        while(a.size() > 0) {
            res.add(a.remove(a.size() - 1));
        }
        return res;
    }

    public static void main(String[] args) {
        ArrayList<Integer> in = new ArrayList(Arrays.asList(1, 2, 3, 4, 5));
        ArrayList<Integer> out = reversed(in);
        ArrayList<Integer> expected = new ArrayList<Integer>(Arrays.asList(5, 4, 3, 2, 1));
        System.out.println(out);
        System.out.println(expected);
        System.out.println(out.equals(expected));
    }
}
