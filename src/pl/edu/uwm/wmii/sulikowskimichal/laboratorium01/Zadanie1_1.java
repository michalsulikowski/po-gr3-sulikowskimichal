package pl.edu.uwm.wmii.sulikowskimichal.laboratorium01;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie1_1 {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.print("Podaj liczbe liczb: ");
        int n = reader.nextInt();

        ArrayList<Double> liczby = new ArrayList<Double>(n);
        for (int i = 0; i < n; i++) {
            liczby.add(reader.nextDouble());
        }

        System.out.print("A) ");
        System.out.println(liczby.stream().reduce(0.0, (acc, elem) -> acc + elem));

        System.out.print("B) ");
        System.out.println(liczby.stream().reduce(1.0, (acc, elem) -> acc * elem));

        System.out.print("C) ");
        System.out.println(liczby.stream().reduce(0.0, (acc, elem) -> acc + Math.abs(elem)));

        System.out.print("D) ");
        System.out.println(liczby.stream().reduce(0.0, (acc, elem) -> acc + Math.sqrt(Math.abs(elem))));

        System.out.print("E) ");
        System.out.println(liczby.stream().reduce(1.0, (acc, elem) -> acc * Math.abs(elem)));
    }
}